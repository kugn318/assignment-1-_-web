
// a JSON database of all the roles and their details, updated with image source
var ictDatabase = [
    {
        "role": "Software Developers & Programmers",
        "seek": 590,
        "salary_max": 100,
        "salary_min": 72,
        "skills": "* computer software and systems\n" +
                                "* programming languages and techniques\n" +
                                "* software development processes such as Agile\n" +
                                "* confidentiality, data security and data protection issues.\n",
        "description": "Software developers and programmers develop and maintain computer software, websites and software applications (apps).",
        "image_source": "../img/SDP.jpg"
    },

    {
        "role": "Database & Systems Administrators",
        "seek": 74,
        "salary_max": 90,
        "salary_min": 66,
        "skills": "* a range of database technologies and operating systems\n" +
        "* new developments in databases and security systems\n" +
        "* computer and database principles and protocols\n",
        "description": "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures.",
        "image_source": "../img/DSA.jpg"
    },

    {
        "role": "Help Desk & IT Support",
        "seek": 143,
        "salary_max": 65,
        "salary_min": 46,
        "skills": "* computer hardware, software, networks and websites\n" +
                                "* the latest developments in information technology.\n",
        "description": "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software.",
        "image_source": "../img/HDIS.jpg"
    },

    {
        "role": "Data Analyst",
        "seek": 270,
        "salary_max": 128,
        "salary_min": 69,
        "skills": "* data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R\n" +
                                "* data analysis, mapping and modelling techniques\n" +
                                "* analytical techniques such as data mining\n",
        "description": "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims.",
        "image_source": "../img/DA.jpg"
    },

    {
        "role": "Test Analyst",
        "seek": 127,
        "salary_max": 98,
        "salary_min": 70,
        "skills": "* programming methods and technology\n" +
                                "* computer software and systems\n" +
                                "* project management\n",
        "description": "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems.",
        "image_source": "../img/TA.jpg"
    },

    {
        "role": "Project Management",
        "seek": 188,
        "salary_max": 190,
        "salary_min": 110,
        "skills": "* principles of project management\n" +
                                "* approaches and techniques such as Kanban and continuous testing\n" +
                                "* how to handle software development issues\n" +
                                "* common web technologies used by the scrum team.\n",
        "description": "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress.",
        "image_source": "../img/PM.jpg"
    }

];


// a JS method to display image and job title/skills/description. Triggered by onclick event.
function makeOnclick(number) {
    var title = document.getElementById("job_title");
    var para1 = document.getElementById("description");
    var para2 = document.getElementById("skill");
    var image = document.getElementById("role_image");


    title.innerText = ictDatabase[number].role + " - Job Description";
    para1.innerText = ictDatabase[number].description;
    para2.innerText = ictDatabase[number].skills;

    image.src = ictDatabase[number].image_source;
    image.alt = ictDatabase[number].role;
}

// a JS method to access JSON database, then prints output as a table. Triggred on pageload
function makeTable() {
    var table = document.getElementById("bigTable");

    var headings = document.getElementById("headings");

    styleTable();

    var cell1 = document.createElement("th");
    cell1.innerHTML = "Role";
    var cell2 = document.createElement("th");
    cell2.innerHTML = "Jobs On Seek";
    var cell3 = document.createElement("th");
    cell3.innerHTML = "Salary Max";
    var cell4 = document.createElement("th");
    cell4.innerHTML = "Salary Min";

    headings.appendChild(cell1);
    headings.appendChild(cell2);
    headings.appendChild(cell3);
    headings.appendChild(cell4);
    table.appendChild(headings);

    for (var i = 0; i < ictDatabase.length; i++) {

        var cell1 = document.createElement("td");
        cell1.innerHTML = ictDatabase[i].role;
        var cell2 = document.createElement("td");
        cell2.innerHTML = ictDatabase[i].seek;
        var cell3 = document.createElement("td");
        cell3.innerHTML = ictDatabase[i].salary_max;
        var cell4 = document.createElement("td");
        cell4.innerHTML = ictDatabase[i].salary_min;

        var row = document.createElement("tr");

        row.id = i;

        row.onclick = function () {makeOnclick(this.id)};

        row.appendChild(cell1);
        row.appendChild(cell2);
        row.appendChild(cell3);
        row.appendChild(cell4);
        table.appendChild(row);
    }

    generateFinalRow();

}




// a helper method to generate & calculate the final row in the table (total of jobs on seek and average max/min salary)
function generateFinalRow(){

    var table = document.getElementById("bigTable");

    var blankCell = document.createElement("td");
    var totalCell = document.createElement("td");
    var averageMaxCell = document.createElement("td");
    var averageMinCell = document.createElement("td");
    var lastRow = document.createElement("tfoot");

    var totalCellValue = Number("0");
    var averageMaxCellValue = Number("0");
    var averageMinCellValue = Number("0");

    for (var i = 0; i < ictDatabase.length; i++) {
        totalCellValue = totalCellValue + ictDatabase[i].seek;
        averageMaxCellValue = averageMaxCellValue + ictDatabase[i].salary_max;
        averageMinCellValue = averageMinCellValue + ictDatabase[i].salary_min;
    }

    averageMaxCellValue = averageMaxCellValue/ictDatabase.length;
    averageMinCellValue = averageMinCellValue/ictDatabase.length;

    totalCell.innerText = "Total: " + totalCellValue;
    averageMaxCell.innerText = "Average: " + averageMaxCellValue.toFixed(2);
    averageMinCell.innerText = "Average: " + averageMinCellValue.toFixed(2);

    lastRow.appendChild(blankCell);
    lastRow.appendChild(totalCell);
    lastRow.appendChild(averageMaxCell);
    lastRow.appendChild(averageMinCell);

    table.appendChild(lastRow);
}

// a helper method to style the table. Used before adding css, could be moved into css but I want to leave here since I spent so much time on this
function styleTable() {
    var table = document.getElementById("bigTable");
    var table_rows = table.getElementsByTagName("tr");
    var table_cells = null;

    table.style.borderStyle = "solid";

    for (var i = 0; i < table_rows.length; i++){
            table_rows[i].style.border = "thin solid black";
    }
}




